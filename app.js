const { Client, RichEmbed } = require("discord.js")
const db = require("redis").createClient()
const client = new Client()

module.exports = {
    client: client,
    db: db,
    embed: RichEmbed
}